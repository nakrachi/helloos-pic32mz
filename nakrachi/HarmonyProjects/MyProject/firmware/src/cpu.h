/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

 
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#ifndef _EXAMPLE_FILE_NAME_H    /* Guard against multiple inclusion */
#define _EXAMPLE_FILE_NAME_H


 
    
    typedef uint32_t REG_TYPE;  
    
    typedef struct _cpuContext {
      
        REG_TYPE at;
        REG_TYPE v0;
        REG_TYPE v1;
        REG_TYPE a0;
        REG_TYPE a1;
        REG_TYPE a2;
        REG_TYPE a3;
        REG_TYPE t0;
        REG_TYPE t1;
        REG_TYPE t2;
        REG_TYPE t3;
        REG_TYPE t4;
        REG_TYPE t5;
        REG_TYPE t6;
        REG_TYPE t7;
        REG_TYPE s0;
        REG_TYPE s1;
        REG_TYPE s2;
        REG_TYPE s3;
        REG_TYPE s4;
        REG_TYPE s5;
        REG_TYPE s6;
        REG_TYPE s7;
        REG_TYPE t8;
        REG_TYPE t9;
       // REG_TYPE r26; // k0
       // REG_TYPE r27; // k1
        REG_TYPE gp;
        REG_TYPE sp;
        REG_TYPE s8;
        REG_TYPE ra;
        REG_TYPE hi;
        REG_TYPE lo;
        REG_TYPE epc;
        REG_TYPE status;
        REG_TYPE SRSCtl;
    } cpuContext_t;


#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
