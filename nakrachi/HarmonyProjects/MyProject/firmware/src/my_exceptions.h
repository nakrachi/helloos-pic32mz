/* 
 * File:   my_exceptions.h
 * Author: nakrachi
 *
 * Created on January 2, 2022, 1:55 PM
 */

#ifndef MY_EXCEPTIONS_H
#define	MY_EXCEPTIONS_H

#ifdef	__cplusplus
extern "C" {
#endif


 
.macro	__saveCPUContext

        // get the address of the current CPU
	
	mfc0  k1, _CP0_EPC
	lw    k0, curCPU
	sw    k1,OFFSET_EPC(k0)
	mfc0  k1, _CP0_STATUS
	// make sure to reenable interrupts, otherwise we will not get a timer interrupt
	//or    k1, 0x00000001
	//and   k1, k1, ~(0x400) // !! CAREFULL !! not sure  about this one, I clear the Core timer IPL but not sure if it's the right thing, or should we allow nested interrupts ? needs to be investigated
	sw    k1,OFFSET_STATUS(k0)
	ins   k1, zero, 1, 15
	ori   k1, k1, 1024
	mtc0  k1, _CP0_STATUS
	
        di // disable interrupts 
	
	/*mfc0  k1, _CP0_STATUS
	// make sure to reenable interrupts, otherwise we will not get a timer interrupt
	or    k1, 0x00000001
	and   k1, k1, ~(0x400) // !! CAREFULL !! not sure  about this one, I clear the Core timer IPL but not sure if it's the right thing, or should we allow nested interrupts ? needs to be investigated
	sw    k1,OFFSET_STATUS(k0)*/
	
	
	mfc0  k1, _CP0_SRSCTL
	sw    k1, OFFSET_SRSCTL(k0)
	
	sw    at_reg,OFFSET_AT(k0)
	sw    v0, OFFSET_V0(k0)
	sw    v1, OFFSET_V1(k0)
	sw    a0, OFFSET_A0(k0)
	sw    a1, OFFSET_A1(k0)
	sw    a2, OFFSET_A2(k0)
	sw    a3, OFFSET_A3(k0)
	sw    t0, OFFSET_T0(k0)
	sw    t1, OFFSET_T1(k0)
	sw    t2, OFFSET_T2(k0)
	sw    t3, OFFSET_T3(k0)
	sw    t4, OFFSET_T4(k0)
	sw    t5, OFFSET_T5(k0)
	sw    t6, OFFSET_T6(k0)
	sw    t7, OFFSET_T7(k0)
	
	sw    s0, OFFSET_S0(k0)
	sw    s1, OFFSET_S1(k0)
	sw    s2, OFFSET_S2(k0)
	sw    s3, OFFSET_S3(k0)
	sw    s4, OFFSET_S4(k0)
	sw    s5, OFFSET_S5(k0)
	sw    s6, OFFSET_S6(k0)
	sw    s7, OFFSET_S7(k0)
	
	sw    t8, OFFSET_T8(k0)
	sw    t9, OFFSET_T9(k0)
	
	// k0 and k1 are not saved for now, the assumption is that it is only used by the kernel, we'll see.
	
	sw    sp, OFFSET_SP(k0)
	
	//switch to core stack
	li    sp, CORE_STACK_HIGH
	
	sw    s8, OFFSET_S8(k0)
	sw    ra, OFFSET_RA(k0)
	mfhi  k1 
	sw    k1,OFFSET_HI(k0)
	mflo  k1
	sw    k1,OFFSET_LO(k0)
	
//	ei 
	nop
	nop
	
	//jr	ra
	//.end __saveCPUContext
  .endm
 

#ifdef	__cplusplus
}
#endif

#endif	/* MY_EXCEPTIONS_H */

