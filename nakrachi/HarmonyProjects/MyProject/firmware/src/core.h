#ifndef __CORE_H__
#define __CORE_H__
#include <stdint.h>
#include <stdbool.h>

typedef struct Task {
    uint32_t* entry;
    uint32_t stackSize;
    uint32_t status;
} task_t;


#define TASKS_COUNT 3
void __createTask(task_t task);
bool __initCore(void);
bool isCoreInitialized();
void __startCore(void);

#endif //__CORE_H__