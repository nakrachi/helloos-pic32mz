/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "cpu.h"
#include "core.h"
#include <stdio.h>
/*
 * mTask1 : prints hello MRTOS and yields to Idle  
 */

extern void __restoreCPUContext(void);
extern void __saveCPUContext(void);
char buff2[] = "> Task #1 \n\r";
void mTask1(void)
{
    while(true)
    {
        // print hello MRTOS
      //  UART2_Write(&buff2[0],sizeof(buff2));
        printf(buff2);
     //  CORETIMER_DelayUs(100);
    }
}
char buff1[] = "> Task #2 \n\r";
void mTask2(void)
{
    while(true)
    {
     //UART1_Write(&buff1[0],sizeof(buff1));
       printf(buff1);
     // CORETIMER_DelayUs(100);
    }
}
uint32_t* idleStack = NULL;
#define IDLE_STACK_SIZE 1024
uint32_t *curCPU = NULL ;

void putch(unsigned char data)
{
//    UART1_WriteByte(data);
}
char buff[] = "> Idle \n\r";
void mIdle(void)
{
   
    while(true)
    {
//         UART1_Write(&buff[0],sizeof(buff));
    //      UART2_Write(&buff[0],sizeof(buff));
     //      putch('a');
 //       UART2_Write(&buff[0],sizeof(buff));
        printf(buff);
//        CORETIMER_DelayUs(100);
    }
}




// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    if(__initCore() == false )
    {
        return ( EXIT_FAILURE );
    }
    
   
    
    
    // start the core
    
    __startCore();
    
    while ( true )
    {
        // NEVER GET HERE
    }

    /* Execution should not come here during normal operation */

    
}


/*******************************************************************************
 End of File
*/

