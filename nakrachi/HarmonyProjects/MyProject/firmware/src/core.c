#include "core.h"
#include "stdlib.h"
#include "device.h"
#include "cpu.h"
#include "peripheral/coretimer/plib_coretimer.h"

typedef struct TCB {
    uint32_t* entry;
    uint32_t* cpu;
    
} tcb_t;

static tcb_t* TCBs;
static int task_count = 0;
extern uint32_t* curCPU;
extern void __restoreCPUContext(void);
extern void __saveCPUContext(void);
static bool coreInitStatus = false;

bool __initCore(void)
{
    // allocate memory for tasks vector 
   TCBs = (tcb_t*) aligned_alloc(4,sizeof(tcb_t) * TASKS_COUNT);
   // allocate memory for the curCPU
  // curCPU = (cpuContext_t*) malloc(sizeof(cpuContext_t));
   if(TCBs != NULL)
   {
       coreInitStatus = true;
   }
   else
       coreInitStatus = false;
    // create tasks 
    
   
    
   return coreInitStatus;
}
bool isCoreInitialized()
{
    return coreInitStatus;
}
extern void mIdle(void);
extern void mTask1(void);
extern void mTask2(void);
 
void __coreTask(void)
{

     task_t task1;
    task1.entry = (uint32_t*) &mIdle;
    task1.stackSize = 2048;
    task1.status = 0x25000001;
    __createTask(task1);
    
    task_t task2;
    task2.entry = (uint32_t*) &mTask1;
    task2.stackSize = 2048;
    task2.status = 0x25000001;
    __createTask(task2);
    
    task_t task3;
    task3.entry = (uint32_t*) &mTask2;
    task3.stackSize = 2048;
    task3.status = 0x25000001;
    __createTask(task3);
    
    curCPU =   TCBs[0].cpu;
    CORETIMER_Start();
    //mIdle();
   __restoreCPUContext();
   
   while(true)
   {
   }
}
#define CORE_STACK_HIGH  0x80020000
#define CORE_STACK_SIZE  0x400
void __startCore(void)
{
    // select the first task to be run
    cpuContext_t* cpu_tmp = (cpuContext_t*) malloc(sizeof(cpuContext_t));
    cpu_tmp->epc = (REG_TYPE) &__coreTask;
    cpu_tmp->sp  =  CORE_STACK_HIGH ; // (REG_TYPE) aligned_alloc(4,sizeof(uint32_t)*1024) -  1024  ;
    cpu_tmp->status = 0x25000001;
    cpu_tmp->gp = 0x80008370;
    cpu_tmp->SRSCtl = 0x1C000000;
    curCPU =  (uint32_t*) cpu_tmp;
    //mIdle();
   __restoreCPUContext();
   
    
}

void __scheduler(void)
{
    CORE_TIMER_InterruptHandler();
    
    static uint8_t task_idx = 0;
    task_idx++; //select next task
    if(task_idx >= task_count)
        task_idx = 0; // wrap around
    curCPU =   TCBs[task_idx].cpu;
    tcb_t tmp_tcb= TCBs[task_idx];
    (void) tmp_tcb;
    
}

void __initStack(uint32_t* sp, uint16_t size)
{
    uint16_t i;
    for(i=0;i<size;i++)
    {
        sp[i] = 0xDEADBEEF;
    }
}

void __createTask(const task_t task)
{
    static uint16_t last_stack_size = CORE_STACK_SIZE;
    static uint32_t last_stack_high = CORE_STACK_HIGH;
    if(task_count >= TASKS_COUNT)
    {
        return; // error
    }
    TCBs[task_count].entry = task.entry;
    cpuContext_t* cpu_tmp = (cpuContext_t*) malloc(sizeof(cpuContext_t));
    cpu_tmp->epc = (REG_TYPE) task.entry;
    cpu_tmp->sp  = last_stack_high - last_stack_size - task.stackSize;// (REG_TYPE) aligned_alloc(4,sizeof(uint32_t)*task.stackSize) -  task.stackSize  ;
    cpu_tmp->status = task.status;
    cpu_tmp->gp = 0x80008370;
    cpu_tmp->SRSCtl = 0x1C000000;
   // __initStack((uint32_t*)cpu_tmp->sp,task.stackSize );
    // only increment if the stack was successfully created 
  //  if(TCBs[task_count].cpu != NULL )       
    {
//        curCPU =  TCBs[task_count].cpu;
        TCBs[task_count].cpu = (uint32_t*) cpu_tmp;
        last_stack_size = task.stackSize ;
        last_stack_high = cpu_tmp->sp;
        task_count++;
    }
    
}